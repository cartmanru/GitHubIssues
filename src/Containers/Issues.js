import React, { Component } from "react";
import { connect } from "react-redux";
import { List } from 'antd';

class Issues extends Component {
  render() {
    return (
      <div>
        <List
          itemLayout="horizontal"
          dataSource={this.props.issues}
          renderItem={item => {
            return (
              <List.Item style={{ cursor: "pointer"}}>
                <List.Item.Meta
                  title={<div>{item.title}</div>}
                  description={item.state}
                />
              </List.Item>
            )
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    issues: state.issues.issues,
  }
};

const mapDispatchToProps = function (dispatch) {
  return {
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Issues);