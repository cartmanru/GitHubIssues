import apisauce from 'apisauce'

const create = (baseURL = 'https://api.github.com') => {

  const api = apisauce.create({
    baseURL,
    timeout: 10000
  })

  const getRepos = (fetchQuery) => api.get('/search/repositories', fetchQuery);
  const getIssues = (fetchQuery) => api.get('/search/issues', fetchQuery);

  return {
    getRepos,
    getIssues,
  }
}

export default {
  create
}