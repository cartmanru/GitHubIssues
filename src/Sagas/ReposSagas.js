import { call, put } from 'redux-saga/effects'
import ReposActions from '../Redux/ReposRedux'

export function * ReposRequest (api, action) {
  const response = yield call(api.getRepos, action.fetchQuery);

  if (response.ok) {
    console.log(response.data);
    yield put(ReposActions.reposSuccess(response.data.items));
  } else {
    yield put(ReposActions.reposFail())
  }
}
