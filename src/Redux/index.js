import {combineReducers} from 'redux'
import configureStore from './CreateStore'
import rootSaga from '../Sagas/'

export default (API) => {
  /* ------------- Assemble The Reducers ------------- */
  const rootReducer = combineReducers({
    repos: require('./ReposRedux').reducer,
    issues: require('./IssuesRedux').reducer,
  });

  return configureStore(rootReducer, rootSaga.bind(null, API));
}

//Use this in every saga from now on
export const getCurrentState = (state) => {
  return state;
}
