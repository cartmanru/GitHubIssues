import React, { Component } from "react";
import { connect } from "react-redux";
import { List, Spin, Input } from 'antd';

import ReposActions from '../Redux/ReposRedux'
import IssuesActions from '../Redux/IssuesRedux'

class Repos extends Component {
  fetchIssues(item){
    this.props.setActiveRepo(item.id);
    this.props.requestIssues({q:`repo:${item.owner.login}/${item.name}`});
  }

  search(text){
    this.props.requestRepos({q:text});
  }

  render() {
    return (
      <div className="heightFull">
        <Input.Search
          placeholder="Поиск"
          onSearch={value => this.search(value)}
          enterButton
        />
        {
          !this.props.fetchingRepos ? (
            <List
              itemLayout="horizontal"
              dataSource={this.props.repos}
              renderItem={item => {
                let itemClassName = "";
                if(item.id === this.props.activeRepo)
                  itemClassName = "active";
                return (
                  <List.Item className={itemClassName} style={{ cursor: "pointer"}} onClick={()=>this.fetchIssues(item)}>
                    <List.Item.Meta
                      title={<div>{item.name}</div>}
                    />
                  </List.Item>
                )
              }}
            />
          ) : (<div style={{position:'relative', height:'100%', paddingTop:70}}><Spin style={{position:'absolute', top:'40%', left:'50%'}} size="large" /></div>)
        }

      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    repos: state.repos.repos,
    activeRepo: state.repos.activeRepo,
    fetchingRepos: state.repos.fetching,
  }
};

const mapDispatchToProps = function (dispatch) {
  return {
    requestRepos: (fetchQuery) => dispatch(ReposActions.reposRequest(fetchQuery)),
    requestIssues: (fetchQuery) => dispatch(IssuesActions.issuesRequest(fetchQuery)),
    setActiveRepo: (id) => dispatch(ReposActions.setActiveRepo(id)),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Repos);