import {createReducer, createActions} from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  reposRequest: ['fetchQuery'],
  reposSuccess: ['repos'],
  setActiveRepo: ['repo'],
  reposFail: null,
})

export const ReposTypes = Types;
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  repos: [],
  activeRepo:null,
  fetching: false,
  error: null,
});

/* ------------- Reducers ------------- */

// request answers for a specific query (user, posts, date)
export const request = (state, {fetchQuery}) =>
  state.merge({fetching: true, fetchQuery});

// successful lookup
export const success = (state, {repos}) =>
  state.merge({fetching: false, error: null, repos});

export const setActiveRepo = (state, {repo}) =>
  state.merge({activeRepo:repo});


// failed to get the publications for feed. How to handle fetch object?? Revert back?
export const failure = (state) =>
  state.merge({fetching: false, error: true});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.REPOS_REQUEST]: request,
  [Types.SET_ACTIVE_REPO]: setActiveRepo,
  [Types.REPOS_SUCCESS]: success,
  [Types.REPOS_FAIL]: failure
})
