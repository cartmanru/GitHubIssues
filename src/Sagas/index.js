import { takeLatest, all } from 'redux-saga/effects'

/* ------------- Types(Redux Constants) ------------- */

import { ReposTypes } from '../Redux/ReposRedux'
import { IssuesTypes } from '../Redux/IssuesRedux'

/* ------------- Sagas ------------- */

import { ReposRequest } from '../Sagas/ReposSagas'
import { IssuesRequest } from '../Sagas/IssuesSagas'

/* ------------- Connect Types To Sagas ------------- */

export default function * root (api) {
  yield all([
    takeLatest(ReposTypes.REPOS_REQUEST, ReposRequest, api),
    takeLatest(IssuesTypes.ISSUES_REQUEST, IssuesRequest, api),

  ])
}
