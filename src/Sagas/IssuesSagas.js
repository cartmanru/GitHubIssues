import { call, put } from 'redux-saga/effects'
import IssuesActions from '../Redux/IssuesRedux'

export function * IssuesRequest (api, action) {
  const response = yield call(api.getIssues, action.fetchQuery);

  if (response.ok) {
    console.log(response.data);
    yield put(IssuesActions.issuesSuccess(response.data.items));
  } else {
    yield put(IssuesActions.issuesFail())
  }
}
