import React, { Component } from "react";
import { connect } from "react-redux";
import '../App.css';

import { Card } from 'antd';

import Repos from './Repos'
import Issues from './Issues'


class RootContainer extends Component {
  render() {
    return (
      <div className='container'>
        <div className='repos'>
          <Card title="Репозитории" bordered={false}>
            <Repos/>
          </Card>
        </div>
        <div className='menu'><h3>Работа с Репо</h3></div>
        <div className='issues'>
          {
            this.props.activeRepo ? (<Card title="Репо Issues" bordered={false}><Issues/></Card>) : null
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    activeRepo: state.repos.activeRepo,
  }
};

const mapDispatchToProps = function (dispatch) {
  return {
  }
};

export default connect( mapStateToProps, mapDispatchToProps)(RootContainer);