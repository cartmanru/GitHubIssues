import {createReducer, createActions} from 'reduxsauce'
import Immutable from 'seamless-immutable'

/* ------------- Types and Action Creators ------------- */

const {Types, Creators} = createActions({
  issuesRequest: ['fetchQuery'],
  issuesSuccess: ['issues'],
  setActiveIssue: ['issue'],
  issuesFail: null,
})

export const IssuesTypes = Types;
export default Creators

/* ------------- Initial State ------------- */

export const INITIAL_STATE = Immutable({
  issues: [],
  activeIssue:null,
  fetching: false,
  error: null,
});

/* ------------- Reducers ------------- */

// request answers for a specific query (user, posts, date)
export const request = (state, {fetchQuery}) =>
  state.merge({fetching: true, fetchQuery});

// successful lookup
export const success = (state, {issues}) =>
  state.merge({fetching: false, error: null, issues});

export const setActiveIssue = (state, {issue}) =>
  state.merge({activeIssue:issue});


// failed to get the publications for feed. How to handle fetch object?? Revert back?
export const failure = (state) =>
  state.merge({fetching: false, error: true});

/* ------------- Hookup Reducers To Types ------------- */

export const reducer = createReducer(INITIAL_STATE, {
  [Types.ISSUES_REQUEST]: request,
  [Types.SET_ACTIVE_ISSUE]: setActiveIssue,
  [Types.ISSUES_SUCCESS]: success,
  [Types.ISSUES_FAIL]: failure
})
